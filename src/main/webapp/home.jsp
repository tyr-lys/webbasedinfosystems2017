<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 29-11-17
  Time: 13:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>My Home page</title>
    <link rel="stylesheet" type="text/css" href="css/site.css">

</head>
<body>
    <div class="headerdiv">
        <div class="headerelement" id="headerTitle">
            <jsp:include page="/includes/pageHeader.jsp"></jsp:include>
        </div>
        <div class="headerelement" id="loginForm">
            <c:choose>
                <c:when test="${sessionScope.user == null}">
                    <jsp:include page="includes/loginForm.jsp">
                        <jsp:param name="error" value="${requestScope.login_error}" />
                    </jsp:include>
                </c:when>
                <c:otherwise>
                    <jsp:include page="includes/greeting.jsp">
                        <jsp:param name="userName" value="${user.userName}" />
                    </jsp:include>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="pageContent">
        <h1>My contents</h1>
    </div>
    <div id="footer">
        <jsp:include page="includes/footer.jsp">
            <jsp:param name="todays_saying" value="Beter een dode kakkerlak in de tuin dan een levende in de keuken!" />
        </jsp:include>
    </div>

</body>
</html>
