<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 29-11-17
  Time: 13:50
  To change this template use File | Settings | File Templates.
--%>
<c:if test="${param.error != null}">
    <h4>${param.error}</h4>
</c:if>

<form action="<c:url value = "/newLogin.do"/>" method="POST">
    <label class="loginlabel" for="#username_field"> User name: </label>
    <input id="username_field" type="text" name="username" required/> <br/>
    <label class="loginlabel" for="#password_field"> User password: </label>
    <input id="password_field" type="password" name="password" required/><br/>
    <label class="login_field"> </label>
    <input type="submit" value="OK"/>
</form>
