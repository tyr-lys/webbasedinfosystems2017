package nl.bioinf.wis1.dao;

public class MyDataSourceException extends Exception {
    public MyDataSourceException(String s) {
        super(s);
    }
}
