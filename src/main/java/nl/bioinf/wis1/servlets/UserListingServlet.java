package nl.bioinf.wis1.servlets;

import nl.bioinf.wis1.users.Address;
import nl.bioinf.wis1.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "UserListingServlet", urlPatterns = "/list.users")
public class UserListingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User numberOne = createUserOne();
        request.setAttribute("userOne", numberOne);

        List<User> registeredUsers = getRegisteredUsers();
        request.setAttribute("registered_users", registeredUsers);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("UserListing.jsp");
        requestDispatcher.forward(request, response);
    }

    private User createUserOne() {
        User one = new User("Henk", "Henk");
        one.setEmail("Henk@example.com");
        Address address = new Address(7, "a", "9701 DA", "Zernikeplein", "Groningen");
        one.setAddress(address);
        return one;
    }

    public List<User> getRegisteredUsers() {
        List<User> users = new ArrayList<>();

        User u = new User("Henk", "Henk");
        u.setEmail("Henk@example.com");
        Address address = new Address(7, "a", "9701 DA", "Zernikeplein", "Groningen");
        u.setAddress(address);
        users.add(u);

        u = new User("Piet", "Piet");
        u.setEmail("Piet@example.com");
        address = new Address(8, "II", "9701 DA", "Zernikepark", "Groningen");
        u.setAddress(address);
        users.add(u);

        u = new User("Roos", "Roos");
        u.setEmail("Roos@example.com");
        address = new Address(17,"9701 DA", "Zernikeplein", "Groningen");
        u.setAddress(address);
        users.add(u);

        return users;
    }
}
